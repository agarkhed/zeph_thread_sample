#include <zephyr.h>
#include <logging/log.h>

#include "THREAD1/thread1.h"
#include "THREAD2/thread2.h"

/* Register to use Logging framework */
LOG_MODULE_REGISTER(main, LOG_LEVEL_INF);

/* Statically define and initialize a message queue */
#define MAIN_THREAD_MAX_MSG_QUEUE 2
K_MSGQ_DEFINE(main_thread_msgq, sizeof(uint8_t), MAIN_THREAD_MAX_MSG_QUEUE, 1);

/* Thread IDs */
extern k_tid_t thread1_id;
extern k_tid_t thread2_id;



void
main(void)
{
    int rc = 0;
    uint8_t main_msg = 0;

    LOG_INF("Started Firmware");

    /* Start Thread 1 */
    rc = start_thread1();
    if (rc == 0) {
        LOG_INF("Started Thread 1");
    } else {
        LOG_INF("Failed to start Thread 1");

        /* Exit main on failure to start thread */
        return;
    }

    /* Start Thread 2 */
    rc = start_thread2();
    if (rc == 0) {
        LOG_INF("Started Thread 2");
    } else {
        LOG_INF("Failed to start Thread 2");

        /* Stop the Thread 1 */
        k_thread_abort(thread1_id);

        /* Exit main on failure to start thread */
        return;
    }

    /* Start the Thread1 with initial delay */
    signal_thread1(1);

    /* Forever loop which exits if any other thread exits */
    while (1) {

        /* Wait for message if any thread exits */
        k_msgq_get(&main_thread_msgq, &main_msg, K_FOREVER);

        /* Ensure that both threads are stopped */
        k_thread_abort(thread1_id);
        k_thread_abort(thread2_id);

        /* Code should not come here, but if it does, return main thread */
        return;
    }
}