#ifndef THREAD2_H
#define THREAD2_H

int start_thread2(void);
int signal_thread2(uint8_t thread1_delay);

#endif /* THREAD1_H */