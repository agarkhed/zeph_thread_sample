#include <zephyr.h>
#include <logging/log.h>

#include "../THREAD1/thread1.h"

/* Register to use with logging module */
LOG_MODULE_DECLARE(main);

/* Thread id of Thread 1 */
k_tid_t thread2_id;

/* Thread struct */
static struct k_thread thread2_obj;

/* Priority of thread2 */
#define THREAD2_PRIORITY 2

/* Size of stack area used by each thread */
#define THREAD2_STACKSIZE 256
K_THREAD_STACK_DEFINE(THREAD2_STACK_AREA, THREAD2_STACKSIZE);

/* Statically define and initialize a message queue */
#define THREAD2_MAX_MSG_QUEUE 2
K_MSGQ_DEFINE(thread2_msgq, sizeof(uint8_t), THREAD2_MAX_MSG_QUEUE, 1);

/* Message main thread on exit */
extern struct k_msgq main_thread_msgq;


/* Thread2 Task */
static void
thread2_task(void *arg1, void *arg2, void *arg3)
{
    uint8_t thread2_msg = 0;

    while (1) {

        /* Wait for msg */
        k_msgq_get(&thread2_msgq, &thread2_msg, K_FOREVER);

        /* Signal Thread1 */
        signal_thread1(thread2_msg);

        /* Sleep for 1 second */
        k_sleep(K_SECONDS(1));

        /* Print Thread2's message */
        LOG_INF("Thread 2");
    }

    /* Code should not come here, but signal main that thread exited */
    k_msgq_put(&main_thread_msgq, &thread2_msg, K_NO_WAIT);
}


/* Helper function to start Thread2 */
int
start_thread2(void)
{
    /* Creating Thread 2 and run it */
    thread2_id =
        k_thread_create(&thread2_obj, THREAD2_STACK_AREA, K_THREAD_STACK_SIZEOF(
        THREAD2_STACK_AREA), thread2_task, NULL, NULL, NULL, THREAD2_PRIORITY, K_ESSENTIAL,
        K_NO_WAIT);

    /* If Thread ID is returned, Thread2 started, return success */
    if (thread2_id != 0) {
        k_thread_name_set(thread2_id, "THREAD2");

        return 0;
    }

    /* Return Thread2 not started */
    return 1;
}


/* Helper function to signal thread2 */
int
signal_thread2(uint8_t thread1_delay)
{
    int rc = 0;

    /* Signal Thread2 MsgQ */
    rc = k_msgq_put(&thread2_msgq, &thread1_delay, K_NO_WAIT);

    return rc;
}