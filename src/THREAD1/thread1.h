#ifndef THREAD1_H
#define THREAD1_H

/* #include <zephyr/types.h> */

int start_thread1(void);
int signal_thread1(uint8_t thread_delay);

#endif /* THREAD1_H */