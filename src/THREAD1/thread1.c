#include <zephyr.h>
#include <logging/log.h>

#include "../THREAD2/thread2.h"

/* Register to use with logging module */
LOG_MODULE_DECLARE(main);

/* Thread id of Thread 1 */
k_tid_t thread1_id;

/* Thread struct */
static struct k_thread thread1_obj;

/* Priority of thread1 */
#define THREAD1_PRIORITY 1

/* Size of stack area used by each thread */
#define THREAD1_STACKSIZE 256
K_THREAD_STACK_DEFINE(THREAD1_STACK_AREA, THREAD1_STACKSIZE);

/* Statically define and initialize a message queue */
#define THREAD1_MAX_MSG_QUEUE 2
K_MSGQ_DEFINE(thread1_msgq, sizeof(uint8_t), THREAD1_MAX_MSG_QUEUE, 1);

/* Message main thread on exit */
extern struct k_msgq main_thread_msgq;


/* Thread1 Task */
static void
thread1_task(void *arg1, void *arg2, void *arg3)
{
    uint8_t thread1_delay = 0;

    while (1) {

        /* Wait for msg */
        k_msgq_get(&thread1_msgq, &thread1_delay, K_FOREVER);

        /* Sleep for the thread delay */
        k_sleep(K_SECONDS(thread1_delay));

        /* Print Thread2's message */
        LOG_INF("Thread 1", thread1_delay);

        /* Increment the thread1_delay */
        thread1_delay++;
        if (thread1_delay > 10) {
            thread1_delay = 1;
        }

        /* Signal Thread2 to print */
        signal_thread2(thread1_delay);
    }

    /* Code should not come here, but signal main that thread exited */
    k_msgq_put(&main_thread_msgq, &thread1_delay, K_NO_WAIT);
}



/* Helper function to start Thread1 */
int
start_thread1(void)
{
    /* Creating Thread 1 and run it */
    thread1_id =
        k_thread_create(&thread1_obj, THREAD1_STACK_AREA, K_THREAD_STACK_SIZEOF(
        THREAD1_STACK_AREA), thread1_task, NULL, NULL, NULL, THREAD1_PRIORITY, K_ESSENTIAL,
        K_NO_WAIT);

    /* If Thread ID is returned, Thread1 started, return success */
    if (thread1_id != 0) {
        k_thread_name_set(thread1_id, "THREAD1");

        return 0;
    }

    /* Return Thread1 not started */
    return 1;
}


/* Helper function to signal thread1 */
int
signal_thread1(uint8_t thread_delay)
{
    int rc = 0;

    /* Signal thread1 */
    rc = k_msgq_put(&thread1_msgq, &thread_delay, K_NO_WAIT);

    return rc;
}