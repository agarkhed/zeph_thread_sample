Thread Sample
=============

Build and Execute
-----------------
1. If Zephyr Project is not created, create a zephyr project using the instructions from Zephyr Getting Started (https://docs.zephyrproject.org/2.7.0/getting_started/index.html).
2. Clone the repository inside the root of the Zephyr Project created in step 1.
3. Build the code using command -
    > \$ cd zeph\_thread\_sample/ <br/>
    > \$ west build -p auto -b native-posix .
4. Execute the code using command -
    > $ ./build/zephyr/zephyr.exe